package com.TestBTSid.testBTSid;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TestBtSidApplication {

	public static void main(String[] args) {
		SpringApplication.run(TestBtSidApplication.class, args);
	}

}
