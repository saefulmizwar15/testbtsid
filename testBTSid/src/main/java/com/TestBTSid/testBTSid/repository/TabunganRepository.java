package com.TestBTSid.testBTSid.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.TestBTSid.testBTSid.model.TabunganModel;


public interface TabunganRepository extends JpaRepository<TabunganModel, Long> {
	@Query(value = "SELECT * FROM tabungan_tbl WHERE nik=:nik", nativeQuery=true)
	List<TabunganModel> getnik(@Param("nik") String nik);

	@Query(value = "SELECT * FROM tabungan_tbl WHERE id=:id", nativeQuery=true)
	TabunganModel getid(@Param("id") Long id);

	@Query(value = "delete * FROM tabungan_tbl WHERE id=:id", nativeQuery=true)
	List<TabunganModel> deleteId(@Param("id") Long id);
	
}
