package saefulProjectSpringBoot;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;

@SpringBootApplication
@EnableJpaAuditing
public class TestBtSidApplicationTests {
	public static void main(String[] x) {
		SpringApplication.run(TestBtSidApplicationTests.class,x);
	}

}